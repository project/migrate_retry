CONTENTS OF THIS FILE

* Requirements
* Installation
* Configuration
* Maintainers

INSTALLATION
------------

Install Migrate retry module in a usual way.
After that, add these lines to the settings.php.

```
$settings['queue_service_migrate_retry'] = 'queue.migrate_retry';
```

Otherwise, the failed migrated items will never stop
to be re-migrated as the retries wouldn't be decremented.

CONFIGURATION
-------------
On the configuration page, check which migrations do you want
to use the migrate retry system.

HOW IT WORKS
-----------

To make the migrate items to be retried,
it is needed to throw a MigrateException with the needs
retry status(\Drupal\migrate_retry\MigrateIdMapInterface::STATUS_NEEDS_RETRY),
when the items are failing.

Only those rows having the source_row_status value
as needs retry will be retried.

There is an example about how to throw the exception
at [the migrate destination used for tests](https://git.drupalcode.org/project/migrate_retry/-/blob/2.0.x/tests/migrate_retry_tests/src/Plugin/migrate/destination/MigrateRetryForceFail.php).

After marking the row with that status
it will be enqueued by the cron,
and a queue worker will re-migrate the item.

It is also possible to enqueue items to retry using the queue manager service:

```php
// Ids of a migrate row in which sourceid1 is 1 and sourceid2 is 34.
$source_ids = [1, 34]
$migration_id = 'my_custom_migration';
\Drupal::service('migrate_retry.queue_manager')
    ->enqueueMigrateRow($migration_id, $source_ids);
```

MAINTAINERS
------------
Omar Mohamad - El Hassan Lopesino - https://www.drupal.org/u/omarlopesino

Sady Sierralta - https://www.drupal.org/u/sadysierralta
