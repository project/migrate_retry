<?php

namespace Drupal\migrate_retry\Event;

/**
 * Defines migrate retry events.
 *
 * @see \Drupal\migrate_retry\Event\MigrateRetryPrepareRowEvent
 */
final class MigrateRetryEvents {

  /**
   * Name of the event fired when preparing a source data row.
   *
   * This event is specifically used in
   * Drupal\migrate_retry\EventSubscriber\MigrateRetryPrepareRowSubscriber,
   * this should not be used outside this module, use migrate_plus module
   * in order to use a dedicated event related to migrate prepare row.
   *
   * This event is dispatched in migrate_retry_migrate_prepare_row() because is
   * the only found way to check that the row hash being processed belongs to
   * the row hash wanted to be processed.
   *
   * @Event
   *
   * @see \Drupal\migrate_retry\Event\MigrateRetryPrepareRowEvent
   *
   * @var string
   */
  const PREPARE_ROW = 'migrate_retry.prepare_row';

}
