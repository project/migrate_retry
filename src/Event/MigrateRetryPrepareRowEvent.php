<?php

namespace Drupal\migrate_retry\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\migrate\Row;

/**
 * Event info of a item being prepared before retrying a migration.
 */
class MigrateRetryPrepareRowEvent extends Event {

  /**
   * Migrate row.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * Constructs the event.
   *
   * @param \Drupal\migrate\Row $row
   *   Migrate item row.
   */
  public function __construct(Row $row) {
    $this->row = $row;
  }

  /**
   * Gets the migrate row.
   *
   * @return \Drupal\migrate\Row
   *   Migrate item row.
   */
  public function getRow() {
    return $this->row;
  }

}
