<?php

namespace Drupal\migrate_retry\EventSubscriber;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate_retry\Event\MigrateRetryPrepareRowEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate_retry\Event\MigrateRetryEvents;

/**
 * Migrate retry event subscriber.
 */
class MigrateRetryPrepareRowSubscriber implements EventSubscriberInterface {

  /**
   * Current hash to be processed.
   *
   * @var string
   */
  protected string $currentHash;

  /**
   * React to a new row.
   *
   * @param \Drupal\migrate_retry\Event\MigrateRetryPrepareRowEvent $event
   *   The prepare-row event.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   */
  public function onPrepareRow(MigrateRetryPrepareRowEvent $event): void {
    if (!empty($this->currentHash)) {
      $hash = $event->getRow()->getHash();
      if ($hash == $this->currentHash) {
        throw new MigrateSkipRowException('Skipped, due to migrate retry id list.', FALSE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      MigrateRetryEvents::PREPARE_ROW => ['onPrepareRow'],
    ];
  }

  /**
   * Set the current hash.
   *
   * @param string $current_hash
   *   The current hash.
   */
  public function setCurrentHash(string $current_hash): void {
    $this->currentHash = $current_hash;
  }

}
