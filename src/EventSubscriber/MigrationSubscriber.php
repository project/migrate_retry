<?php

namespace Drupal\migrate_retry\EventSubscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate_retry\MigrateIdMapInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Shows during a migration that there are items needing retry.
 */
class MigrationSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Logger, in case there are items needing retry.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Database to query for pending source ids.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * MigrationSubscriber constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   */
  public function __construct(LoggerChannelInterface $logger, Connection $database) {
    $this->logger = $logger;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      MigrateEvents::POST_IMPORT => 'onPostImport',
    ];
  }

  /**
   * Show items needing retry after executing a migration.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The import event.
   */
  public function onPostImport(MigrateImportEvent $event) {
    $id_map = $event->getMigration()->getIdMap();
    if (!$id_map instanceof Sql) {
      throw new \InvalidArgumentException('Only migrations with SQL id map are supported.');
    }
    $rows_needing_retry_count = $this->getRowsNeedingRetry($id_map);
    if ($rows_needing_retry_count > 0) {
      $this->logger->warning($this->formatPlural($rows_needing_retry_count,
        'There is an item needing retry for this migration.', 'There are @numItems items needing retry for this migration.',
        [
          '@numItems' => $rows_needing_retry_count,
        ]
      ));
    }
  }

  /**
   * Get rows that needs retry.
   *
   * @param \Drupal\migrate\Plugin\migrate\id_map\Sql $id_map
   *   SQL id map.
   */
  protected function getRowsNeedingRetry(Sql $id_map) {
    $table_name = $id_map->getQualifiedMapTableName();
    return $this->database->select($table_name, 'map')
      ->condition('source_row_status', MigrateIdMapInterface::STATUS_NEEDS_RETRY)
      ->countQuery()->execute()->fetchField();
  }

}
