<?php

namespace Drupal\migrate_retry\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Migrate retry settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const CRON_INTERVAL_CONFIG_NAME = 'cron_interval';
  const DELAY_TIME_CONFIG_NAME = 'delay_time';

  /**
   * Migrate plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigratePluginManagerInterface
   */
  protected $migratePluginManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_retry_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_retry.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setMigratePluginManager($container->get('plugin.manager.migration'));
    return $instance;
  }

  /**
   * Set the migrate plugin manager.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migratePluginManager
   *   Migrate plugin manager.
   */
  public function setMigratePluginManager(MigrationPluginManagerInterface $migratePluginManager) {
    $this->migratePluginManager = $migratePluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->config('migrate_retry.settings');

    $form['cron'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron Settings'),
    ];

    $form['cron'][self::CRON_INTERVAL_CONFIG_NAME] = [
      '#type' => 'select',
      '#title' => $this->t('Cron interval'),
      '#options' => [
        '-1' => $this->t('Every cron run'),
        '3600' => $this->t('Every hour'),
        '7200' => $this->t('Every 2 hours'),
        '10800' => $this->t('Every 3 hours'),
        '14400' => $this->t('Every 4 hours'),
        '21600' => $this->t('Every 6 hours'),
        '28800' => $this->t('Every 8 hours'),
        '43200' => $this->t('Every 12 hours'),
        '86400' => $this->t('Every 24 hours'),
      ],
      '#default_value' => empty($configuration->get(self::CRON_INTERVAL_CONFIG_NAME)) ? 3600 : $configuration->get(self::CRON_INTERVAL_CONFIG_NAME),
      '#description' => $this->t('How often should the items that needs retry be enqueued?'),
      '#required' => TRUE,
    ];

    $migrations_list = $this->getMigrationsOptionList();
    if (!empty($migrations_list)) {
      $form['migrations'] = [
        '#title' => $this->t('What migrations needs retry?'),
        '#type' => 'checkboxes',
        '#options' => $migrations_list,
        '#default_value' => $configuration->get('migrations'),
      ];
    }
    else {
      $form['migrations'] = [
        '#markup' => $this->t('You need to have at least one migrate migration in order to use this module.'),
      ];
    }

    $form[self::DELAY_TIME_CONFIG_NAME] = [
      '#title' => $this->t('Delay time'),
      '#description' => $this->t('How long to wait to try to migrate again this item.'),
      '#type' => 'select',
      '#options' => [
        900 => $this->t('15 minutes'),
        1800 => $this->t('30 minutes'),
        3600 => $this->t('1 hour'),
        21600 => $this->t('6 hours'),
        86400 => $this->t('1 day'),
      ],
      '#required' => TRUE,
      '#default_value' => $configuration->get(self::DELAY_TIME_CONFIG_NAME),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get migrations available to get its retries.
   *
   * @return array
   *   Migrate option list.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getMigrationsOptionList() : array {
    $option_list = [];
    foreach ($this->migratePluginManager->getDefinitions() as $definition) {
      $migration_id = $definition['id'];
      /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
      $migration = $this->migratePluginManager->createInstance($migration_id);
      try {
        if ($migration->getSourcePlugin() instanceof RequirementsInterface) {
          $migration->getSourcePlugin()->checkRequirements();
        }
      }
      catch (\Exception $e) {
        // Migration is not properly set up, excluding.
        continue;
      }

      $option_list[$migration_id] = $definition['label'] . ' (' . $migration_id . ')';
    }
    return $option_list;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('migrate_retry.settings')
      ->set('migrations', array_filter($form_state->getValue('migrations') ?? []))
      ->set(self::CRON_INTERVAL_CONFIG_NAME, $form_state->getValue(self::CRON_INTERVAL_CONFIG_NAME))
      ->set(self::DELAY_TIME_CONFIG_NAME, $form_state->getValue(self::DELAY_TIME_CONFIG_NAME))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
