<?php

namespace Drupal\migrate_retry;

use Drupal\migrate\Plugin\MigrateIdMapInterface as OriginalMigrateIdMapInterface;

/**
 * Migrate id map for rows needing retry.
 *
 * @package Drupal\migrate_retry
 */
interface MigrateIdMapInterface extends OriginalMigrateIdMapInterface {

  /**
   * Rows that failed when importing due to a temporary problem.
   */
  const STATUS_NEEDS_RETRY = 255;

}
