<?php

namespace Drupal\migrate_retry;

use Drupal\Core\Queue\DelayedRequeueException;

/**
 * Thrown when trying to import, but the migration is not idle.
 */
class MigrationBusyException extends DelayedRequeueException {
}
