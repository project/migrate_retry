<?php

namespace Drupal\migrate_retry\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;
use Drupal\migrate_retry\MigrateIdMapInterface;
use Drupal\migrate_retry\MigrationBusyException;
use Drupal\migrate_retry\RetryFailedException;
use Drupal\migrate\MigrateExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate_retry\EventSubscriber\MigrateRetryPrepareRowSubscriber;

/**
 * Defines 'migrate_retry' queue worker.
 *
 * @QueueWorker(
 *   id = "migrate_retry",
 *   title = @Translation("MigrateRetry"),
 *   cron = {"time" = 60}
 * )
 */
class MigrateRetry extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Machine name of the queue.
   *
   * @var string
   */
  const QUEUE_NAME = 'migrate_retry';

  /**
   * Allowed retries.
   *
   * @var int
   */
  const MAX_RETRIES = 5;

  /**
   * Queue, used to requeue in case of error.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Used to load the migrations so we do the retries.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * Used to get the delay time.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Prepare row subscriber.
   *
   * @var \Drupal\migrate_retry\EventSubscriber\MigrateRetryPrepareRowSubscriber
   */
  protected $prepareRowSubscriber;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.migration'),
      $container->get('queue'),
      $container->get('config.factory'),
      $container->get('migrate_retry.prepare_row_subscriber')
    );
  }

  /**
   * Constructs the migrate retry queue worker.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param array $plugin_definition
   *   Plugin definition.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migrationPluginManager
   *   Loads any migration to migrate the elements that needs retry.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Loads the migrate retry queue.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\migrate_retry\EventSubscriber\MigrateRetryPrepareRowSubscriber $prepare_row_subscriber
   *   Prepare row subscriber.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    MigrationPluginManagerInterface $migrationPluginManager,
    QueueFactory $queueFactory,
    ConfigFactoryInterface $configFactory,
    MigrateRetryPrepareRowSubscriber $prepare_row_subscriber) {
    $this->migrationPluginManager = $migrationPluginManager;
    $this->queue = $queueFactory->get(self::QUEUE_NAME);
    $this->configFactory = $configFactory;
    $this->prepareRowSubscriber = $prepare_row_subscriber;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Check the item will be processed is valid.
   *
   * @param mixed $data
   *   Data.
   *
   * @return bool
   *   TRUE when the retry information is available:
   *   - Source ids
   *   - Migration name.
   *   - Retries (it should be higher than 0).
   */
  protected function validItem($data) : bool {
    return is_object($data) && isset($data->migration_id)
      && isset($data->source_id_values)
      && is_array($data->source_id_values)
      && isset($data->retries) && is_int($data->retries) && $data->retries > 0;
  }

  /**
   * Handle an exception when the migration still fails.
   *
   * @param object $data
   *   Queued item.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   Migration.
   */
  protected function handleException(\stdClass $data, MigrationInterface $migration) {
    if ($data->retries > 0) {
      throw new RetryFailedException($this->configFactory->get('migrate_retry.settings')->get('delay_time'), 'Element failed to import');
    }
    else {
      $row = $this->buildRow($data, $migration);
      $migration->getIdMap()->saveIdMapping($row, [], MigrateIdMapInterface::STATUS_FAILED);
    }
  }

  /**
   * Build migrate row for the queue item.
   *
   * @param object $data
   *   Queued item.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   Migration.
   *
   * @return \Drupal\migrate\Row
   *   Row built from source id values.
   */
  protected function buildRow(\stdClass $data, MigrationInterface $migration) : Row {
    $ids = $migration->getSourcePlugin()->getIds();
    $source_values = [];
    $i = 0;
    foreach ($ids as $key => $value) {
      $source_values[$key] = $data->source_id_values[$i];
      $i++;
    }
    return new Row($source_values, $source_values);
  }

  /**
   * Do the data import.
   *
   * @param object $data
   *   Data retrieved by queue.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function doImport(\stdClass $data) {
    $migration = $this->migrationPluginManager->createInstance($data->migration_id);
    if ($migration instanceof MigrationInterface) {
      if ($migration->getStatus() == MigrationInterface::STATUS_IDLE) {
        $row = $this->buildRow($data, $migration);
        $source_row = $migration->getIdMap()->getRowBySource($row->getSourceIdValues());
        if (!empty($source_row) && $source_row['source_row_status'] == MigrateIdMapInterface::STATUS_IMPORTED) {
          return;
        }

        $source_id_values = $row->getSourceIdValues();
        $migration->getIdMap()->setUpdate($source_id_values);
        $this->prepareRowSubscriber->setCurrentHash($row->getHash());
        $executable = new MigrateExecutable($migration, new MigrateMessage());
        $data->retries--;
        $executable->import();

        $row = $migration->getIdMap()->getRowBySource($source_id_values);
        if (!empty($row['source_row_status']) && $row['source_row_status'] !== MigrateIdMapInterface::STATUS_IMPORTED) {
          $this->handleException($data, $migration);
        }
      }
      else {
        $data->retries--;
        throw new MigrationBusyException($this->configFactory->get('migrate_retry.settings')->get('delay_time'), 'The migration with name ' . $migration->label() . ' is not idle.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if ($this->validItem($data)) {
      $this->doImport($data);
    }
  }

}
