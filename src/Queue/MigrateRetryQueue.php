<?php

namespace Drupal\migrate_retry\Queue;

use Drupal\Core\Queue\DatabaseQueue;

/**
 * Migrate retry queue implementation.
 *
 * @ingroup queue
 */
class MigrateRetryQueue extends DatabaseQueue {

  /**
   * {@inheritdoc}
   */
  public function delayItem($item, int $delay) {
    // Only allow a positive delay interval.
    if ($delay < 0) {
      throw new \InvalidArgumentException('$delay must be non-negative');
    }

    try {
      // Add the delay relative to the current time.
      $expire = \Drupal::time()->getCurrentTime() + $delay;
      // Update the expiry time of this item.
      $update = $this->connection->update(static::TABLE_NAME)
        ->fields([
          'expire' => $expire,
          // It is needed to update the data
          // so that the retries are decremented.
          'data' => serialize($item->data),
        ])
        ->condition('item_id', $item->item_id);
      return (bool) $update->execute();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      // If the table doesn't exist we should consider the item nonexistent.
      return TRUE;
    }
  }

}
