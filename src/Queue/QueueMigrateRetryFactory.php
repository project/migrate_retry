<?php

namespace Drupal\migrate_retry\Queue;

use Drupal\Core\Queue\QueueDatabaseFactory;

/**
 * Defines factory for migrate retry queue.
 *
 * @todo hook requirements to warn the uses that migrate retry is not
 * using the correct queue class.
 */
class QueueMigrateRetryFactory extends QueueDatabaseFactory {

  /**
   * Constructs a new queue object for a given name.
   *
   * @param string $name
   *   The name of the collection holding key and value pairs.
   *
   * @return \Drupal\migrate_retry\Queue\MigrateRetryQueue
   *   A key/value store implementation for the given $collection.
   */
  public function get($name) {
    return new MigrateRetryQueue($name, $this->connection);
  }

}
