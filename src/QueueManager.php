<?php

namespace Drupal\migrate_retry;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\QueueFactory;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Drupal\migrate_retry\Plugin\QueueWorker\MigrateRetry;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Enqueue those migrate items that needs a retry.
 */
class QueueManager implements QueueManagerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Queries items needing retry.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Migrate retry logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $migrateRetryLoggerChannel;

  /**
   * Constructs a QueueManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, QueueFactory $queue_factory, MigrationPluginManagerInterface $migration_plugin_manager, ModuleHandlerInterface $module_handler, Connection $database, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->queueFactory = $queue_factory;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->moduleHandler = $module_handler;
    $this->database = $database;
    $this->migrateRetryLoggerChannel = $logger_factory->get('migrate_retry');
  }

  /**
   * Get the migrations that are set up for retrying.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[]
   *   Migrations.
   */
  protected function getMigrations() : array {
    $migrations = [];
    $migration_ids = $this->configFactory->get('migrate_retry.settings')->get('migrations');
    foreach ($migration_ids as $migration_id) {
      $migration = $this->migrationPluginManager->createInstance($migration_id);

      if (!$migration instanceof MigrationInterface) {
        continue;
      }

      // Prevent including items from  migration wich are not idle, in some case
      // migrations might end up stuck from previous executions, meaning that
      // unless migrate status is reset, items from that migration won't be
      // processed as expected.
      if ($migration->getStatus() != MigrationInterface::STATUS_IDLE) {
        $this->migrateRetryLoggerChannel->notice('Items from migration with id ":migration_id", will not be enqueued because is not idle.', [':migration_id' => $migration->id()]);
        continue;
      }

      try {
        if ($migration->getSourcePlugin() instanceof RequirementsInterface) {
          $migration->getSourcePlugin()->checkRequirements();
        }
      }
      catch (\Exception $e) {
        // Migration is not properly set up, excluding.
        \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('error'), $e), fn() => watchdog_exception('error', $e));
        continue;
      }

      $migrations[] = $migration;
    }

    return $migrations;
  }

  /**
   * {@inheritdoc}
   */
  public function canEnqueue() {
    return $this->queueFactory->get(MigrateRetry::QUEUE_NAME)->numberOfItems() === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function enqueueRowsNeedingRetry() {
    foreach ($this->getMigrations() as $migration) {
      $id_map = $migration->getIdMap();
      if (!$id_map instanceof Sql) {
        throw new \InvalidArgumentException('Only migrations with SQL id map are supported.');
      }
      $rows_needing_retry = $this->getRowsNeedingRetry($id_map);
      foreach ($rows_needing_retry as $row_needing_retry) {
        $source_id_values = [];
        foreach (get_object_vars($row_needing_retry) as $key => $value) {
          if (strpos($key, 'sourceid') === 0) {
            $source_id_values[] = $value;
          }
        }
        $this->enqueueMigrateRow($migration->id(), $source_id_values);
      }
    }
  }

  /**
   * Get list of rows needing retry.
   *
   * @param \Drupal\migrate\Plugin\migrate\id_map\Sql $id_map
   *   SQL id map.
   *
   * @return array
   *   List of objects with each migrate item data.
   */
  protected function getRowsNeedingRetry(Sql $id_map) {
    return $this->database->select($id_map->getQualifiedMapTableName(), 'map')
      ->fields('map')
      ->condition('source_row_status', MigrateIdMapInterface::STATUS_NEEDS_RETRY)
      ->execute()
      ->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function enqueueMigrateRow($migration_id, array $source_id_values) {
    $this->moduleHandler->invokeAll('migrate_retry_enqueue_migrate_row', [
      $migration_id,
      $source_id_values,
    ]);
    $data = [
      'migration_id' => $migration_id,
      'source_id_values' => array_values($source_id_values),
      'retries' => MigrateRetry::MAX_RETRIES,
    ];
    $this->queueFactory->get(MigrateRetry::QUEUE_NAME)->createItem((object) $data);
  }

}
