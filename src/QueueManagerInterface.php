<?php

namespace Drupal\migrate_retry;

/**
 * QueueManager service.
 */
interface QueueManagerInterface {

  /**
   * Enqueue a migrate row.
   *
   * @param string $migration_id
   *   Migration id.
   * @param array $source_id_values
   *   Row source id values.
   *
   * @NOTE: should migration id needs to be
   * in the list of migrations that needs retry?
   */
  public function enqueueMigrateRow($migration_id, array $source_id_values);

  /**
   * Enqueue the rows that needs to be retried.
   */
  public function enqueueRowsNeedingRetry();

  /**
   * Check if it's possible to enqueue.
   *
   * @return bool
   *   TRUE if enqueueing is possible.
   */
  public function canEnqueue();

}
