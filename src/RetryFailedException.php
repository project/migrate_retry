<?php

namespace Drupal\migrate_retry;

use Drupal\Core\Queue\DelayedRequeueException;

/**
 * Dispatched when a item is trying to be reimport, and it fails.
 *
 * @package Drupal\migrate_retry
 */
class RetryFailedException extends DelayedRequeueException {
}
