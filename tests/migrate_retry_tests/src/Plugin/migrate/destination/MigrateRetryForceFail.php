<?php

namespace Drupal\migrate_retry_tests\Plugin\migrate\destination;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_retry\MigrateIdMapInterface;

/**
 * Force migration to fail so that retries can be tested.
 *
 * @MigrateDestination(
 *   id = "migrate_retry_force_fail"
 * )
 */
class MigrateRetryForceFail extends DestinationBase {

  /**
   * If true the migration will fail.
   *
   * @var bool
   */
  public static $forceFail = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $force_fail = &drupal_static('migrate_retry_force_fail', TRUE);

    if ($force_fail) {
      throw new MigrateException('Row migration failed', 0, NULL, MigrationInterface::MESSAGE_ERROR, MigrateIdMapInterface::STATUS_NEEDS_RETRY);
    }

    return [
      'id' => 1,
    ];
  }

}
