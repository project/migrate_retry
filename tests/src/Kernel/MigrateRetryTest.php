<?php

namespace Drupal\migrate_retry\Tests;

use Drupal\Core\Queue\DatabaseQueue;
use Drupal\Core\Queue\DelayableQueueInterface;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_retry\MigrateIdMapInterface;
use Drupal\migrate_retry\Plugin\QueueWorker\MigrateRetry;
use Drupal\Tests\migrate\Kernel\MigrateTestBase;
use Drupal\migrate_retry\RetryFailedException;

/**
 * Tests the core behaviours of migrate retry.
 *
 * @group migrate_retry
 */
class MigrateRetryTest extends MigrateTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'migrate_retry',
    'migrate_retry_tests',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('migrate_retry');


    \Drupal::service('config.factory')
      ->getEditable('migrate_retry.settings')
      ->set('migrations', ['migrate_retry_test'])
      ->save();
  }

  /**
   * Set weather the testing destination will fail or not.
   *
   * @param bool $force
   *   TRUE if it is needed to make it fail, FALSE otherwise.
   */
  protected function forceMigrateFail(bool $force) {
    $force_fail = &drupal_static('migrate_retry_force_fail', TRUE);
    $force_fail = $force;
  }

  /**
   * Migrate the testing migration items.
   */
  protected function migrateItems() {
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
    $migration = \Drupal::service('plugin.manager.migration')->createInstance('migrate_retry_test');
    $executable = new MigrateExecutable($migration);
    $executable->import();
  }

  /**
   * Assert a migrated item has a specific status.
   *
   * @param int $source_id
   *   Source id.
   * @param int $expected_status
   *   Expected status.
   */
  protected function assertMigratedItemStatus($source_id, $expected_status) {
    $source_row_status = (int) \Drupal::database()->select('migrate_map_migrate_retry_test', 'm')
      ->fields('m', ['source_row_status'])
      ->condition('sourceid1', $source_id)
      ->execute()
      ->fetchField();

    $this->assertEquals($expected_status, $source_row_status);
  }

  /**
   * Process one item of a specific queue.
   *
   * @param string $queue_name
   *   THe name of the queue.
   */
  protected function processQueueItem($queue_name) {
    \Drupal::database()->update(DatabaseQueue::TABLE_NAME)
      ->fields([
        'expire' => 0,
      ])
      ->condition('expire', 0, '<>')
      ->condition('expire', \Drupal::time()->getCurrentTime(), '<')
      ->execute();

    /** @var \Drupal\Core\Queue\QueueWorkerBase $queue_worker */
    $queue_worker = \Drupal::service('plugin.manager.queue_worker')
      ->createInstance('migrate_retry');
    $queue = \Drupal::queue($queue_name);
    $item = $queue->claimItem($queue_worker->getPluginDefinition()['cron']['time']);
    if (empty($item)) {
      throw new \RuntimeException('No items in the queue');
    }

    try {
      $queue_worker->processItem($item->data);
    }
    catch (RetryFailedException $exception) {
      if ($queue instanceof DelayableQueueInterface) {
        $queue->delayItem($item, $exception->getDelay());
      }
      throw $exception;
    }
  }

  /**
   * Asserts the number of items that a queue contains.
   *
   * @param string $queue_name
   *   Name of the queue.
   * @param int $expected_number
   *   Number of expected elements in the queue.
   */
  protected function assertQueueItemsNumber($queue_name, $expected_number) {
    $queue = \Drupal::queue($queue_name);
    $this->assertEquals($expected_number, $queue->numberOfItems());
  }

  /**
   * Get the expire time of the first queue item.
   *
   * @return int
   *   Timestamp indicating when it will expire.
   */
  protected function getQueuedItemExpire() {
    return \Drupal::database()->select('queue')
      ->fields('queue', ['expire'])
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Checks that items failed are managed by migrate retry.
   */
  public function testMigrateRetry() {
    $this->forceMigrateFail(TRUE);
    $this->migrateItems();
    $this->assertMigratedItemStatus(1, MigrateIdMapInterface::STATUS_NEEDS_RETRY);
    $this->assertMigratedItemStatus(2, MigrateIdMapInterface::STATUS_NEEDS_RETRY);
    migrate_retry_cron();
    $this->assertQueueItemsNumber(MigrateRetry::QUEUE_NAME, 2);

    $this->forceMigrateFail(FALSE);
    $this->processQueueItem(MigrateRetry::QUEUE_NAME);
    $this->processQueueItem(MigrateRetry::QUEUE_NAME);
    $this->assertMigratedItemStatus(1, MigrateIdMapInterface::STATUS_IMPORTED);
    $this->assertMigratedItemStatus(2, MigrateIdMapInterface::STATUS_IMPORTED);
  }

  /**
   * Checks that delayed items are not processed until time passes.
   */
  public function testMigrateRetryDelay() {
    $this->forceMigrateFail(TRUE);

    // Short delay time for a fast check.
    \Drupal::service('config.factory')
      ->getEditable('migrate_retry.settings')
      ->set('delay_time', 1)
      ->save();
    $this->migrateItems();

    $this->assertMigratedItemStatus(1, MigrateIdMapInterface::STATUS_NEEDS_RETRY);
    migrate_retry_cron();
    $this->assertEquals(0, $this->getQueuedItemExpire());
    for ($i = 0; $i < 2; $i++) {
      try {
        $this->processQueueItem(MigrateRetry::QUEUE_NAME);
      }
      catch (\Exception $exception) {
        $this->assertInstanceOf(RetryFailedException::class, $exception);
      }
    }

    $this->assertMigratedItemStatus(1, MigrateIdMapInterface::STATUS_NEEDS_RETRY);
    $queued_item_expire = $this->getQueuedItemExpire();

    // Check that until the delay time passes the queued item won't be migrated.
    $this->assertGreaterThan(\Drupal::time()->getCurrentTime(), $queued_item_expire);
    $this->assertLessThanOrEqual(\Drupal::time()->getCurrentTime()
      + \Drupal::config('migrate_retry.settings')->get('delay_time'), $queued_item_expire);

    try {
      $this->processQueueItem(MigrateRetry::QUEUE_NAME);
      throw new \RuntimeException('Delay not working');
    }
    catch (\RuntimeException $exception) {
      $this->assertEquals('No items in the queue', $exception->getMessage());
    }

    // Check that after the delay time passes the queued item will be migrated.
    while (\Drupal::time()->getCurrentTime() <= $queued_item_expire) {
      usleep(1000);
    }

    $this->assertGreaterThan($queued_item_expire, \Drupal::time()->getCurrentTime());
    $this->forceMigrateFail(FALSE);
    $this->processQueueItem(MigrateRetry::QUEUE_NAME);
    $this->processQueueItem(MigrateRetry::QUEUE_NAME);
    $this->assertMigratedItemStatus(1, MigrateIdMapInterface::STATUS_IMPORTED);

  }

}
